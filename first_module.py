# This is just a stupid short program I made to understand how "if __name__ == '__main__'" works


print("This will always be run")
# print("First Module's name " + __name__)

def main():
    print("First Module's name is " + __name__)

if __name__ == '__main__':
    main()

# if __name__ == '__main__':
#     print("Run directly")
# else:
#     print("Run from import")